using DatingApp.API.Data.Entities;

namespace DatingApp.API.Service
{
    public interface ITokenService
    {
        string CreateToken(string username);
    }
}